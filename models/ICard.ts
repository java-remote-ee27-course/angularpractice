export interface ICard{
    id: number;
    title: string;
    url: string;
    thumbnailUrl:string;
}