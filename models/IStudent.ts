export interface IStudent{
    id?: number;
    name: string;
    lastname: string;
    username:string;
    email: string;
}