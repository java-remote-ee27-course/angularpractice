# AngularPractice

This is an Angular practice project demo. See the [Gitlab page](https://java-remote-ee27-course.gitlab.io/angularpractice)

It is created just for fun to test out different features of Angular and practice creation of components, services, forms etc.

## Uses

- routing
- data binding (interpolation, property binding, event binding, two-way binding)
- *ngFor, *ngIf etc.   
- REST API's GET, POST, PUT, DELETE queries for Angular
- A students form, for demo purpose it is made to fetch data from https://jsonplaceholder.typicode.com/users. It can be easily configured to fetch data from fale json server (see from "Pages" section)
- Contacts page includes a video demo of how Student form works with fake json server (below)


## Pages 

- home - links to other pages
- about - uses REST API to fetch data from https://jsonplaceholder.typicode.com/photos and *ngFor for creating 5000 photo thumbnail cards  
- Contacts web page has a students form, for demo purpose it is made to fetch data from https://jsonplaceholder.typicode.com/users. It can be easily configured to fetch data from fake Json server, to demo adding, deleting, updating students data using REST API. To use fake json server, - - git clone project, 
- - install fake json server from <a href="https://github.com/typicode/json-server" target="_blank">JSON SERVER</a>
- - in services/student.service.ts change URL https://jsonplaceholder.typicode.com/users to http://localhost:3000

![src/assets/students_form.png](src/assets/students_form.png)

![src/assets/students_add_form.png](src/assets/students_add_form.png)

![src/assets/students_edit_form.png](src/assets/students_edit_form.png)

![src/assets/video.png](src/assets/video.png)

## Created by

Katlin Kalde