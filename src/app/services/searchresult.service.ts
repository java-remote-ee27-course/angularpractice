import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICard } from 'models/ICard';


@Injectable({
  providedIn: 'root'
})
export class SearchresultService {
  cardsUrl=`https://jsonplaceholder.typicode.com/photos`;

  constructor(private httpClient: HttpClient) { }

  getCards(): Observable<Array<ICard>>{
    //This gets the JSON with products:  curl -X GET http://localhost:3000/products
    return this.httpClient.get<Array<ICard>>(this.cardsUrl);
  }
}
