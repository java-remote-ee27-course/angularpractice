import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IStudent } from 'models/IStudent';


@Injectable({
  providedIn: 'root'
})
export class StudentService {

  private studentsUrl = 'https://jsonplaceholder.typicode.com/users';
  httpHeaderOptions = { 
    headers: new HttpHeaders({ 'Content-Type' : 'application/json' })
  }

  constructor(private http: HttpClient) {
  }
       
  getStudents(): Observable<IStudent[]> {
    return this.http.get<IStudent[]>(this.studentsUrl);
  }

  getStudent(id: number): Observable<IStudent> {
    const url = `${this.studentsUrl}/${id}`;
    return this.http.get<IStudent>(url);
  }

  updateStudent(student: IStudent): Observable<IStudent>{
    const url = `${this.studentsUrl}/${student.id}`;
    return this.http.put<IStudent>(url, student, this.httpHeaderOptions);
  }

  addStudent(student: IStudent): Observable<IStudent>{
    const url = `${this.studentsUrl}`;
    return this.http.post<IStudent>(url, student);
  } 

  deleteStudent(student: IStudent): Observable<IStudent>{
    const url = `${this.studentsUrl}/${student.id}`;
    return this.http.delete<IStudent>(url);
  }
}
