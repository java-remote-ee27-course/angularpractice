import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { IStudent } from 'models/IStudent';
import { StudentService } from '../../services/student.service';

//Base code: @Software Development Academy, edited by Katlin

@Component({
  selector: 'app-student-edit',
  templateUrl: './student-edit.component.html',
  styleUrls: [ './student-edit.component.css' ]
})
export class StudentEditComponent implements OnInit {
  student: IStudent = {
    id: 0,
    username: '',
    name : '',
    lastname : '',
    email:''
  };

  constructor(
    private route: ActivatedRoute,
    private studentService: StudentService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getStudent();
  }

  getStudent(): void {
    // Obtaining the value of the "id" parameter and converting it to a number
    // +this... (uses unary plus trying to convert value to number)
    //! is assertion of non null value, otherwise TS does not like it:
    const id = +this.route.snapshot.paramMap.get('id')!;

    //get student info first
    this.studentService.getStudent(id)
      .subscribe(student => this.student = student);
  }

  goBack(): void {
    this.location.back();
  }

  //save student info and go to previous student table view
  onSave(): void {
    this.studentService.updateStudent(this.student)
      .subscribe(() => this.goBack());
  }
}
