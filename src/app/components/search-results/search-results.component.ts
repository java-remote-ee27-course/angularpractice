import { Component } from '@angular/core';
import { ICard } from 'models/ICard';
import { SearchresultService } from 'src/app/services/searchresult.service';


@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent {
  cards : Array<ICard> = [];
  errorMessage : string ='';

  card : ICard = {
    id:0,
    title:'',
    url:'',
    thumbnailUrl:'',
  }

  ngOnInit(): void{
    this.getResults();
  }
  constructor(private resultService: SearchresultService){}

  getResults(): void{
    this.resultService.getCards().subscribe({
      next: (result: ICard[]) =>{
        this.cards = result;
      },
      error: (err:any) => {
        console.error(err);
        this.errorMessage = err.status;
      }
    });
  }
}
