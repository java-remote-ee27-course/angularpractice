import { Component, Input } from '@angular/core';
import { ICard } from 'models/ICard';
import { DateTime } from 'luxon';


@Component({
  selector: 'app-tilecard',
  templateUrl: './tilecard.component.html',
  styleUrls: ['./tilecard.component.css']
})
export class TilecardComponent {
  @Input() card: ICard = {
    id:0,
    title:'',
    url:'',
    thumbnailUrl:'',
  }
  dateAndTime : string = '0';
  ngOnInit(){
    this.displayDateTime();
  }
  
  displayDateTime() : void{
    this.dateAndTime = DateTime.now().toLocaleString(DateTime.DATE_FULL);
  }


}
