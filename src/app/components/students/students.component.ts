import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ViewChild } from '@angular/core';
import { IStudent } from 'models/IStudent';
import { StudentService } from '../../services/student.service';


@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent {
  students: Array<IStudent> = [];

  constructor(private studentService: StudentService) { }

  //get students from studentService (StudentService) and 
  //give to this method students here the value of studnets fetched from studentService
  getStudents(): void {
    this.studentService.getStudents().subscribe(students => this.students = students);
  }

  ngOnInit(): void {
    this.getStudents();
  }

  onDelete(student: IStudent): void {
    this.students = this.students.filter(s => s !== student);
    this.studentService.deleteStudent(student).subscribe();
  }

  onAdd(name: string, lastname:string, email: string, username: string) {
    // Removing whitespaces from beg., end if they exist
    name = name.trim();
    lastname = lastname.trim();
    username = username.trim();
    email = email.trim();

    //e-mail validation is checked in html
    this.studentService.addStudent({ name, lastname, email, username } as IStudent)
      .subscribe(student => { this.students.push(student) });

    email = '';
  }


}
